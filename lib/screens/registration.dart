import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../models/user.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _auth = FirebaseAuth.instance;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String firstName, lastName, username, password;
  final TextEditingController _usernameController = TextEditingController();
  bool isChecked = false;

  void _welcomeDiaLog(user) {
    var email = user['email'];
    _snackBar('Welcome $email!');
  }

  void _failMess(message) {
    _snackBar(message);
  }

  void _snackBar(String text) {
    final snackBar = SnackBar(
      content: Text('$text'),
      duration: Duration(seconds: 10),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    _submitRegister(firstName, lastName, username, password) {
      print('username $username firstlast $firstName $lastName');
      if (firstName == null ||
          lastName == null ||
          username == null ||
          password == null) {
        _snackBar('Please input fill in all fields');
      } else if (isChecked == false) {
        _snackBar('Please agree with our terms');
      } else {
        Provider.of<UserModel>(context).createUser(
            username: username,
            password: password,
            firstName: firstName,
            lastName: lastName,
            success: _welcomeDiaLog,
            fail: _failMess);
      }
    }

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: SafeArea(
            child: ListenableProvider<UserModel>(
          create: (_) => UserModel(),
          child: Consumer<UserModel>(builder: (context, value, child) {
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 30.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    // SizedBox(
                    //   height: 70.0,
                    // ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Center(
                              child: Image.asset(
                            'assets/images/NoonLogo.png',
                            width: MediaQuery.of(context).size.width / 4,
                            fit: BoxFit.contain,
                          )),
                        ),
                        IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () => Navigator.of(context).pop()),
                      ],
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text(
                      'إنشاء حساب على نون',
                      style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text(
                      'الاسم الأول',
                      style: TextStyle(color: Colors.grey[400]),
                    ),
                    TextField(
                      onChanged: (value) => firstName = value,
                      decoration: InputDecoration(
                        hintText: 'يرجي إدخال اسم الأول ',
                        hintStyle: TextStyle(fontSize: 20.0,
                        color: Colors.black54),
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text('الاسم العائلة', style: TextStyle(color: Colors.grey[400]),),
                    TextField(
                      onChanged: (value) => lastName = value,
                      decoration: InputDecoration(
                        hintText: 'يرجى إدخال الاسم العائلة ',
                       hintStyle: TextStyle(fontSize: 20.0,
                        color: Colors.black54),
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text('البريد الإلكتروني', style: TextStyle(color: Colors.grey[400]),),
                    TextField(
                      controller: _usernameController,
                      onChanged: (value) => username = value,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        hintText: 'يرجى إدخال البريد الإلكتروني',
                        hintStyle: TextStyle(fontSize: 20.0,
                        color: Colors.black54),
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text('كلمة الرور', style: TextStyle(color: Colors.grey[400]),),
                    TextField(
                      obscureText: true,
                      onChanged: (value) => password = value,
                      decoration: InputDecoration(
                          hintText: 'يرجى إدخال كلمة الرور',
                          hintStyle: TextStyle(fontSize: 20.0,
                        color: Colors.black54),
                          suffixIcon: Icon(Icons.visibility,size: 15.0)),
                      obscuringCharacter: '*',
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    // Row(
                    //   children: <Widget>[
                    //     Checkbox(
                    //       value: isChecked,
                    //       activeColor: Theme.of(context).primaryColor,
                    //       checkColor: Colors.white,
                    //       onChanged: (value) {
                    //         //print(value);
                    //         isChecked = !isChecked;
                    //         setState(() {});
                    //       },
                    //     ),
                    //     Text('I want to create an account',
                    //         style: TextStyle(fontSize: 16.0)),
                    //   ],
                    // ),
                    // SizedBox(
                    //   height: 10.0,
                    // ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Material(
                        color:
                            Colors.brown[100], //Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        elevation: 0,
                        child: MaterialButton(
                          onPressed: () async {
                            try {
                              await _auth.createUserWithEmailAndPassword(
                                  email: username, password: password);
                              print("register on firebase complete");
                            } catch (e) {
                              print(e);
                            }
                            //
                            _submitRegister(
                                firstName, lastName, username, password);
                          },
                          minWidth: 200.0,
                          elevation: 0.0,
                          height: 42.0,
                          child: Text(
                            value.loading == true
                                ? 'جارى التسجيل'
                                : 'اشترك الآن',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'هل لديك حساب بالفعل؟ ',
                            style: TextStyle(color: Colors.black45),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Text(
                              'تسجيل الدخول',
                              style: TextStyle(
                                color: Colors
                                    .black, //Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        )));
  }
}
