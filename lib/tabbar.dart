import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fstore/screens/orders.dart';
import 'package:fstore/screens/products.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'common/config.dart' as config;
import 'common/constants.dart';
import 'common/tools.dart';
import 'generated/i18n.dart';
import 'models/cart.dart';
import 'models/category.dart';
import 'models/product.dart';
import 'models/user.dart';
import 'screens/cart.dart';
import 'screens/categories/index.dart';
import 'screens/home.dart';
import 'screens/user.dart';

class MainTabs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainTabsState();
  }
}

class MainTabsState extends State<MainTabs> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _auth = FirebaseAuth.instance;
  FirebaseUser loggedInUser;
  int pageIndex = 0;
  int currentPage = 0;
  String currentTitle = "Home";
  Color currentColor = Colors.deepPurple;
  bool isAdmin = false;
  List<Widget> _tabView = [
    HomeScreen(),
    CategoriesScreen(),
    UserScreen(),
    MyOrders(), //anaa kuderay si length isugu dheelitiranyo
    WishList(),
  ];

  void getCurrentUser() async {
    try {
      final user = await _auth.currentUser();
      if (user != null) {
        setState(() {
          loggedInUser = user;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  List getChildren(List<Category> categories, Category category) {
    List<Widget> list = [];
    var children = categories.where((o) => o.parent == category.id).toList();
    if (children.length == 0) {
      list.add(
        ListTile(
          leading: Padding(
            child: Text(category.name),
            padding: EdgeInsets.only(left: 20),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios,
            size: 12,
          ),
          onTap: () {
            Product.showList(
                context: context, cateId: category.id, cateName: category.name);
          },
        ),
      );
    }
    for (var i in children) {
      list.add(
        ListTile(
          leading: Padding(
            child: Text(i.name),
            padding: EdgeInsets.only(left: 20),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios,
            size: 12,
          ),
          onTap: () {
            Product.showList(context: context, cateId: i.id, cateName: i.name);
          },
        ),
      );
    }
    return list;
  }

  List showCategories() {
    final categories = Provider.of<CategoryModel>(context).categories;
    List<Widget> widgets = [];

    if (categories != null) {
      var list = categories.where((item) => item.parent == 0).toList();
      for (var index in list) {
        widgets.add(
          ExpansionTile(
            title: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                index.name.toUpperCase(),
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ),
            children: getChildren(categories, index),
          ),
        );
      }
    }
    return widgets;
  }

  bool checkIsAdmin() {
    if (loggedInUser.email == config.adminEmail) {
      isAdmin = true;
    } else {
      isAdmin = false;
    }
    return isAdmin;
  }

  @override
  void initState() {
    getCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var totalCart = Provider.of<CartModel>(context).totalCartQuantity;
    bool loggedIn = Provider.of<UserModel>(context).loggedIn;
    final isTablet = Tools.isTablet(MediaQuery.of(context));

    return Container(
      color: Theme.of(context).backgroundColor,
      child: DefaultTabController(
        length: 5,
        child: Scaffold(
          appBar: AppBar(
            titleSpacing: 0.0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.menu),
                  onPressed: () => Scaffold.of(context).openDrawer(),
                ),
                Expanded(
                  child: Center(
                    child: Image.asset(
                      "assets/images/JoharhLogo.jpg",
                      fit: BoxFit.cover,
                      height: 40.0,
                      width: 50.0,
                    ),
                  ),
                ),
              ],
            ),
            automaticallyImplyLeading: false,
            centerTitle: true,
            actions: <Widget>[
              Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(FontAwesomeIcons.search),
                    onPressed: () {},
                    iconSize: 15.0,
                  ),
                  Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          FontAwesomeIcons.shoppingCart,
                          size: 15.0,
                        ),
                        onPressed: () {},
                      ),
                      Positioned(
                        top: 5.0,
                        left: 5.0,
                        width: 10.0,
                        height: 16.0,
                        child: Text(
                          '0',
                          style: TextStyle(color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                ],
              )
            ],
            backgroundColor: Colors.white,
            elevation: 0.0,
          ),
          backgroundColor: Colors.grey[50], //Theme.of(context).backgroundColor,
          resizeToAvoidBottomPadding: false,
          key: _scaffoldKey,
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: _tabView,
          ),
          drawer: Drawer(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  DrawerHeader(
                    child: Row(
                      children: <Widget>[
                        Image.asset(kLogoImage, height: 38),
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          leading: Icon(
                            Icons.shopping_basket,
                            size: 20,
                          ),
                          title: Text(S.of(context).shop),
                          onTap: () {
                            Navigator.pushReplacementNamed(context, "/home");
                          },
                        ),
                        ListTile(
                          leading: Icon(FontAwesomeIcons.wordpress, size: 20),
                          title: Text(S.of(context).blog),
                          onTap: () {
                            Navigator.pushNamed(context, "/blogs");
                          },
                        ),
                        ListTile(
                          leading: Icon(FontAwesomeIcons.heart, size: 20),
                          title: Text(S.of(context).myWishList),
                          onTap: () {
                            Navigator.pushNamed(context, "/wishlist");
                          },
                        ),
                        ListTile(
                          leading: Icon(Icons.exit_to_app, size: 20),
                          title: loggedIn
                              ? Text(S.of(context).logout)
                              : Text(S.of(context).login),
                          onTap: () {
                            loggedIn
                                ? Provider.of<UserModel>(context).logout()
                                : Intl.withLocale(
                                    'ar',
                                    () =>
                                        Navigator.pushNamed(context, "/login"));
                          },
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        ExpansionTile(
                          initiallyExpanded: true,
                          title: Text(
                            S.of(context).byCategory.toUpperCase(),
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.5),
                            ),
                          ),
                          children: showCategories(),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          bottomNavigationBar: Material(
            color: Colors.black,
            child: TabBar(
              tabs: [
                Tab(
                  icon: Icon(
                    FontAwesomeIcons.home,
                    color: Colors.white,
                    size: 20.0,
                  ),
                  child: Text(
                    'الرئيسية',
                    style: TextStyle(color: Colors.white, fontSize: 12.0),
                  ),
                ),
                Tab(
                  icon: Icon(
                    FontAwesomeIcons.solidEdit,
                    color: Colors.white,
                    size: 20.0,
                  ),
                  child: Text(
                    'الاقسام',
                    style: TextStyle(color: Colors.white, fontSize: 12.0),
                  ),
                ),
                Tab(
                  icon: Icon(
                    FontAwesomeIcons.user,
                    color: Colors.white,
                    size: 20.0,
                  ),
                  child: Text(
                    'حسابي',
                    style: TextStyle(color: Colors.white, fontSize: 12.0),
                  ),
                ),
                Tab(
                  icon: Icon(
                    FontAwesomeIcons.truck,
                    color: Colors.white,
                    size: 15.0,
                  ),
                  child: Text(
                    'ادارة الطلبات',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 8.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Tab(
                  icon: Icon(
                    Icons.favorite,
                    color: Colors.white,
                    size: 20.0,
                  ),
                  child: Text('الرغبات',
                      style: TextStyle(color: Colors.white, fontSize: 12.0)),
                ),
              ],
              isScrollable: false,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorPadding: EdgeInsets.all(4.0),
              indicatorColor: Theme.of(context).primaryColor,
            ),
          ),
        ),
      ),
    );
  }
}
